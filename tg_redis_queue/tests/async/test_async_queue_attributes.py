import pickle
import typing

import pytest

from tg_redis_queue.async_redis_que import AsyncRedisObjectQueue
from tg_redis_queue.tests.utils import queue_name_for_test_run


@pytest.mark.asyncio
async def test_async_expiry_time_can_be_none(redis_url):
    """Check if the queue never expires, if EXPIRY_TIME is set to None"""

    class UnexpiringAsyncRedisObjectQueue(AsyncRedisObjectQueue):
        EXPIRY_TIME = None

    queue = await UnexpiringAsyncRedisObjectQueue.create(
        queue_name_for_test_run("test_async_queue_that_never_expires"), redis_url=redis_url
    )

    try:
        assert await queue.get_expiry_time() is None

        await queue.add({"text": "text"})
        assert await queue.get_expiry_time() is None
    finally:
        # Cleanup the queue from Redis, as it will never expire on it's own
        await queue.prune()
        await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_expiry_time_can_be_changed(redis_url):
    """Check if queue expires as expected"""

    class ExpiringAsyncRedisObjectQueue(AsyncRedisObjectQueue):
        EXPIRY_TIME = 10

    queue = await ExpiringAsyncRedisObjectQueue.create(
        queue_name_for_test_run("test_async_queue_that_expires"), redis_url=redis_url
    )

    assert await queue.get_expiry_time() == 0

    await queue.add({"text": "text"})
    assert await queue.get_expiry_time() > 0

    await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_can_not_create_async_queue_with_constructor(redis_url):
    """Since __init__ can't run async code, special helper method has to be used to create AsyncRedisObjectQueue"""
    with pytest.raises(RuntimeError):
        queue = AsyncRedisObjectQueue(queue_name_for_test_run("test_async_queue_with_constructor"), redis_url=redis_url)


@pytest.mark.asyncio
async def test_custom_data_serializer(redis_url):
    """Check if it is possible to use custom data serializer with async queue"""

    class PickledRedisObjectQueue(AsyncRedisObjectQueue):
        def _get_redis_url(self) -> str:
            return redis_url

        @classmethod
        def dump_data(cls, data: typing.Dict) -> bytes:
            return pickle.dumps(data)

        @classmethod
        def load_data(cls, data: bytes) -> typing.Dict:
            return pickle.loads(data)

    queue = await PickledRedisObjectQueue.create(queue_name_for_test_run("test_pickled_queue"))
    await queue.add({"id": 42})

    assert await queue.get_total_size() == 1

    item = await queue.pop()
    assert item.data["id"] == 42

    await queue.cleanup_connection()
