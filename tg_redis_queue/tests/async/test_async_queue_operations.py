import asyncio

import pytest

from tg_redis_queue import RedisQueItem
from tg_redis_queue.async_redis_que import AsyncRedisObjectQueue
from tg_redis_queue.tests.utils import create_random_async_queue, queue_name_for_test_run


@pytest.mark.asyncio
async def test_async_queue_add_and_get():
    queue = await create_random_async_queue()
    await queue.add({"id": 1})
    assert await queue.get_total_size() == 1

    await queue.add(RedisQueItem({"id": 1}))
    assert await queue.get_total_size() == 2

    first_item = await queue.get()

    first_item_again = await queue.pop()

    assert first_item.key == first_item_again.key

    await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_async_queue_read_empty_queue():
    queue = await create_random_async_queue()
    assert await queue.get() is None
    assert await queue.pop() is None

    await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_async_queue_remove():
    queue = await create_random_async_queue()
    await queue.add({"id": 1})
    await queue.add({"id": 2})
    await queue.add({"id": 3})

    items = await queue.get_items(0, 3)
    assert len(items) == 3

    await queue.remove_item(items[1])
    assert await queue.get_total_size() == 2

    await queue.remove(items)
    assert await queue.get_total_size() == 0

    await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_async_move_to_end():
    queue = await create_random_async_queue()

    await asyncio.gather(
        queue.add({"id": 1}),
        queue.add({"id": 2}),
    )

    await queue.add({"id": 3}),

    items = await queue.get_items()
    assert [item.data["id"] for item in items][-1] == 3

    middle_item = items[1]
    await queue.move_to_end(middle_item)

    items = await queue.get_items()
    assert [item.data["id"] for item in items][-1] in [1, 2]

    await queue.cleanup_connection()


@pytest.mark.asyncio
async def test_async_race(redis_url):
    async def enqueue_data():
        queue = await AsyncRedisObjectQueue.create(name=queue_name_for_test_run("async_race_test"), redis_url=redis_url)

        await asyncio.gather(*[queue.add({"id": i}) for i in range(50)])

        await queue.cleanup_connection()

    async def consume_data():
        queue = await AsyncRedisObjectQueue.create(name=queue_name_for_test_run("async_race_test"), redis_url=redis_url)

        received_ids = set()

        start = asyncio.get_event_loop().time()

        while len(received_ids) < 50 and asyncio.get_event_loop().time() - start < 0.25:
            item = await queue.pop()
            if item:
                received_ids.add(item.data["id"])
            else:
                await asyncio.sleep(0.01)

        assert received_ids == set(i for i in range(50))

        await queue.cleanup_connection()

    await asyncio.gather(
        enqueue_data(),
        consume_data(),
    )
