import random
import string

from tg_redis_queue.tests.conftest import REDIS_URL


def get_random_string():
    return "".join(random.choice(string.ascii_letters) for i in range(32))


RANDOM_TEST_RUN_ID = get_random_string()


def queue_name_for_test_run(name: str):
    return "{id}_{name}".format(id=RANDOM_TEST_RUN_ID, name=name)


def get_random_queue():
    from tg_redis_queue.sync_redis_queue import RedisObjectQueue

    return RedisObjectQueue(
        name=get_random_string(),
        redis_url=REDIS_URL,
    )


async def create_random_async_queue():
    from tg_redis_queue.async_redis_que import AsyncRedisObjectQueue

    return await AsyncRedisObjectQueue.create(name=get_random_string(), redis_url=REDIS_URL)
