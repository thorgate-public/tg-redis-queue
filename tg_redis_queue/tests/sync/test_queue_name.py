import pytest

from tg_redis_queue.sync_redis_queue import RedisObjectQueue
from tg_redis_queue.tests.utils import queue_name_for_test_run


def test_queues_with_same_name_are_same(redis_url):
    """Two instances of queue with same name are same queue (but not same instance)"""
    queue_instance_one = RedisObjectQueue(queue_name_for_test_run("test_queue"), redis_url=redis_url)
    queue_instance_two = RedisObjectQueue(queue_name_for_test_run("test_queue"), redis_url=redis_url)

    assert queue_instance_one is not queue_instance_two
    queue_instance_one.add({"text": "test"})

    assert queue_instance_one.get_total_size() == 1
    assert queue_instance_two.get_total_size() == 1

    item = queue_instance_two.pop()
    assert item.data["text"] == "test"

    assert queue_instance_one.get_total_size() == 0
    assert queue_instance_two.get_total_size() == 0


def test_queues_with_different_names_are_different(redis_url):
    """Two instances of queue with different name are independent"""
    queue_instance_one = RedisObjectQueue(queue_name_for_test_run("test_queue_1"), redis_url=redis_url)
    queue_instance_two = RedisObjectQueue(queue_name_for_test_run("test_queue_2"), redis_url=redis_url)

    assert queue_instance_one is not queue_instance_two
    queue_instance_one.add({"text": "test"})

    assert queue_instance_one.get_total_size() == 1
    assert queue_instance_two.get_total_size() == 0

    item = queue_instance_one.pop()
    assert item.data["text"] == "test"

    assert queue_instance_one.get_total_size() == 0
    assert queue_instance_two.get_total_size() == 0
