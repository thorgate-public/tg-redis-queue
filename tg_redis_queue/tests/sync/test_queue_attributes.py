import pickle
import typing

import pytest

from tg_redis_queue.sync_redis_queue import RedisObjectQueue
from tg_redis_queue.tests.utils import queue_name_for_test_run


def test_redis_url_from_constructor(redis_url):
    """Check if REDIS url is accepted from init argument"""
    queue = RedisObjectQueue(queue_name_for_test_run("test_queue"), redis_url=redis_url)
    assert queue.redis.client_id()


def test_redis_url_from_method(redis_url):
    """Check if REDIS url is accepted from overridden method"""

    class MyRedisObjectQueue(RedisObjectQueue):
        def _get_redis_url(self):
            return redis_url

    queue = MyRedisObjectQueue(queue_name_for_test_run("test_queue"))
    assert queue.redis.client_id()


def test_redis_url_required():
    """Check if REDIS url is required"""

    with pytest.raises(RuntimeError):
        RedisObjectQueue("TestQueue")


def test_expiry_time_can_be_none(redis_url):
    """Check if the queue never expires, if EXPIRY_TIME is set to None"""

    class UnexpiringRedisObjectQueue(RedisObjectQueue):
        EXPIRY_TIME = None

    queue = UnexpiringRedisObjectQueue(queue_name_for_test_run("test_queue_that_never_expires"), redis_url=redis_url)

    try:
        assert queue.get_expiry_time() is None

        queue.add({"text": "text"})
        assert queue.get_expiry_time() is None
    finally:
        # Cleanup the queue from Redis, as it will never expire on it's own
        queue.prune()


def test_expiry_time_can_be_changed(redis_url):
    """Check if queue expires as expected"""

    class ExpiringRedisObjectQueue(RedisObjectQueue):
        EXPIRY_TIME = 10

    queue = ExpiringRedisObjectQueue(queue_name_for_test_run("test_queue_that_expires"), redis_url=redis_url)

    assert queue.get_expiry_time() == 0

    queue.add({"text": "text"})
    assert queue.get_expiry_time() > 0


def test_can_cleanup_connection(redis_url):
    """Cleanup is not needed for synchronous queue, however to be compatible with async queue this method should
    still be available"""
    queue = RedisObjectQueue(queue_name_for_test_run("test_queue"), redis_url=redis_url)
    queue.cleanup_connection()


def test_custom_data_serializer(redis_url):
    """Check if it is possible to use custom data serializer"""

    class PickledRedisObjectQueue(RedisObjectQueue):
        def _get_redis_url(self) -> str:
            return redis_url

        @classmethod
        def dump_data(cls, data: typing.Dict) -> bytes:
            return pickle.dumps(data)

        @classmethod
        def load_data(cls, data: bytes) -> typing.Dict:
            return pickle.loads(data)

    queue = PickledRedisObjectQueue(queue_name_for_test_run("test_pickled_queue"))
    queue.add({"id": 42})

    assert queue.get_total_size() == 1

    item = queue.pop()
    assert item.data["id"] == 42


def test_custom_data_serializer(redis_url):
    """Check if queue expires as expected"""

    class PickledRedisObjectQueue(RedisObjectQueue):
        def _get_redis_url(self) -> str:
            return redis_url

        @classmethod
        def dump_data(cls, data: typing.Dict) -> bytes:
            return pickle.dumps(data)

        @classmethod
        def load_data(cls, data: bytes) -> typing.Dict:
            return pickle.loads(data)

    queue = PickledRedisObjectQueue(queue_name_for_test_run("test_pickled_queue"))
    queue.add({"id": 42})

    assert queue.get_total_size() == 1

    item = queue.pop()
    assert item.data["id"] == 42
