import pytest

from tg_redis_queue import RedisQueItem
from tg_redis_queue.tests.utils import get_random_queue


def test_add_dictionary():
    queue = get_random_queue()

    queue.add(
        {
            "title": "Test",
            "timestamp": 123456789,
        }
    )

    assert queue.get_total_size() == 1

    items = queue.get_items(0, 1)
    assert len(items) == 1
    assert items[0].key
    assert items[0].data["title"] == "Test"
    assert items[0].data["timestamp"] == 123456789


def test_add_item():
    queue = get_random_queue()
    queue.add(
        RedisQueItem(
            data={
                "title": "Test",
                "timestamp": 123456789,
            },
            key="123",
        )
    )

    assert queue.get_total_size() == 1

    items = queue.get_items(0, 1)
    assert len(items) == 1
    assert items[0].key == "123"
    assert items[0].data["title"] == "Test"
    assert items[0].data["timestamp"] == 123456789


def test_add_same_item():
    queue = get_random_queue()

    item = RedisQueItem(data=dict(), key="123")
    queue.add(item)

    assert queue.get_total_size() == 1

    queue.add(item)
    assert queue.get_total_size() == 1

    item.key = "124"
    queue.add(item)
    assert queue.get_total_size() == 2


def test_items_are_in_sequence():
    queue = get_random_queue()

    queue.add({"id": 1})
    queue.add({"id": 2})
    queue.add({"id": 3})

    assert queue.get_total_size() == 3

    items = queue.get_items()
    assert len(items) == 3
    assert items[0].data["id"] == 1
    assert items[1].data["id"] == 2
    assert items[2].data["id"] == 3


def test_read_empty_queue():
    queue = get_random_queue()

    assert queue.get_total_size() == 0

    items = queue.get_items()
    assert len(items) == 0


def test_move_to_end():
    queue = get_random_queue()

    queue.add({"id": 1})
    queue.add({"id": 2})
    queue.add({"id": 3})

    items = queue.get_items()
    assert [item.data["id"] for item in items] == [1, 2, 3]

    middle_item = items[1]
    queue.move_to_end(middle_item)

    items = queue.get_items()
    assert [item.data["id"] for item in items] == [1, 3, 2]


def test_remove_item():
    queue = get_random_queue()
    explicit_item = RedisQueItem({"id": 1})

    queue.add(explicit_item)
    queue.add({"id": 2})
    queue.add({"id": 3})

    implicit_item = queue.get_items()[1]
    assert queue.get_total_size() == 3

    queue.remove([explicit_item, implicit_item])
    assert queue.get_total_size() == 1

    queue.remove_item(explicit_item)
    assert queue.get_total_size() == 1

    assert queue.pop().data["id"] == 3
    assert queue.get_total_size() == 0


def test_remove_junk():
    queue = get_random_queue()

    with pytest.raises(TypeError):
        queue.remove([{"id": 1}])

    with pytest.raises(TypeError):
        queue.remove({"id": 1})

    with pytest.raises(TypeError):
        queue.remove_item({"id": 1})
