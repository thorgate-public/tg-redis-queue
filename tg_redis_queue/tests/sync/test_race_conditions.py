import pytest

import before_after

from tg_redis_queue import RedisQueItem
from tg_redis_queue.tests.utils import get_random_queue


def test_gone_while_reading():
    """It is possible, that item is gone while it is being read"""
    queue = get_random_queue()

    queue.add({"id": 0})
    queue.add({"id": 1})

    item = queue.get_items(0, 1)[0]

    def remove_item_fn(*args, **kwargs):
        queue.remove_item(item)

    with before_after.before("redis.StrictRedis.hget", remove_item_fn):
        popped_item = queue.pop()

    assert not popped_item


def test_gone_while_writing():
    """It is possible, that while queue is gone while adding an item"""
    queue = get_random_queue()

    queue.add({"id": 0})

    def prune_fn(*args, **kwargs):
        queue.prune()

    with before_after.before("tg_redis_queue.sync_redis_queue.RedisObjectQueue._update_expiry_time", prune_fn):
        queue.add({"id": 1})

    assert queue.get_total_size() == 0


def test_gone_while_removing():
    """It is possible, that while one remove operation another remove operation runs"""
    queue = get_random_queue()

    item1 = RedisQueItem({"id": 1})
    item2 = RedisQueItem({"id": 2})
    item3 = RedisQueItem({"id": 3})

    queue.add(item1)
    queue.add(item2)
    queue.add(item3)

    # This will only run once, as per before_after docs
    def remove_one_item(*args, **kwargs):
        queue.remove_item(item2)

    with before_after.before("redis.StrictRedis.hdel", remove_one_item):
        queue.remove([item1, item2, item3])

    assert queue.get_total_size() == 0
