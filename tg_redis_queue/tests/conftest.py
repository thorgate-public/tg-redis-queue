import os

import pytest


# REDIS_PORT is set by gitlab CI, with values like `tcp://172.17.0.3:6379`
REDIS_URL = os.environ.get("REDIS_URL", None) or os.environ.get("REDIS_PORT", "").replace("tcp://", "redis://")
# Fail early if there environment misconfiguration
assert REDIS_URL, "Set REDIS_URL environment variable to run the tests"


@pytest.fixture()
def redis_url():
    return REDIS_URL
