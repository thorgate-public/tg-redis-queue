CYAN ?= \033[0;36m
COFF ?= \033[0m

.PHONY:
all: help

.PHONY:
help:
	@echo -e "+------<<<<                                     Tasks                                    >>>>------+"
	@echo -e "$(CYAN)make test$(COFF)               - Runs automatic tests on your python code"
	@echo -e "$(CYAN)make setup$(COFF)              - Prepares virtual environment for development"
	@echo -e "$(CYAN)make coverage$(COFF)           - Produces coverage report"
	@echo -e "$(CYAN)make quality$(COFF)            - Checks code quality"
	@echo -e "$(CYAN)make black-check-all$(COFF)    - Checks code formatting with black"
	@echo -e "$(CYAN)make black-format-all$(COFF)   - Reformats code with black"
	@echo -e "$(CYAN)make isort$(COFF)              - Checks import order with isort"
	@echo -e "$(CYAN)make isort-fix$(COFF)          - Sorts import order"
	@echo -e "$(CYAN)make prospector$(COFF)         - Analyzes code for problems with prospector"

.PHONY:
setup:
	poetry env use python3.8
	poetry install
	poetry run pip install redis==3.2.0
	poetry run pip install aioredis==1.3.1

.PHONY:
test:
ifndef SKIP_SYNC_TESTS
	PYTHONPATH=$(CURDIR) poetry run pytest tg_redis_queue/tests/sync
endif
ifndef SKIP_ASYNC_TESTS
	PYTHONPATH=$(CURDIR) poetry run pytest tg_redis_queue/tests/async
endif

.PHONY:
coverage:
	PYTHONPATH=$(CURDIR) poetry run coverage run -m pytest
	poetry run coverage report -m

.PHONY:
quality: black-check-all prospector isort

.PHONY:
black-check-all:
	poetry run black --check ./tg_redis_queue

.PHONY:
black-format-all:
	poetry run black ./tg_redis_queue

.PHONY:
isort:
	poetry run isort --recursive --check-only --diff ./tg_redis_queue

.PHONY:
isort-fix:
	poetry run isort --recursive ./tg_redis_queue

.PHONY:
prospector:
	poetry run prospector  --no-autodetect tg_redis_queue
