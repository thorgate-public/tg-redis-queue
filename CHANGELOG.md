# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.3] - 2021-06-30

### Changed

- Reordered pyproject.toml so that poetry sections are on the top
- Made package homepage to be git repository for easier access

### Fixed

- Typo in readme
- Wrong dates in this changelog (we were lost in time, 2020->2021)
- Remove MANIFEST.in since pyproject.toml made it obsolete
- Added missing dataclasses dependency

## [1.1.2] - 2021-06-10

### Added

- More examples in readme on how to manually override the key for item

### Fixed

- Typo in readme in aioredis example
- Links in changelog to show diffs
- Examples providing not accurate data on how get_items upper limit works

## [1.1.1] - 2021-06-10

### Fixed

- Package description gone in pypi

## [1.1.0] - 2021-06-10

### Added

- Asynchronous redis queue implementation
- Ability to override the serialization logic in a subclass (`dump_data` and `load_data`) to 
  allow using some alternative to json
- This changelog

### Changed

- Package no longer depends on redis directly (since asynchronous version uses aioredis), to 
  install with proper dependencies use `synchronous` or `asyncio` extra requirements (see [README](README.md))
- Due to different implementations requiring different packages, is no longer possible to import directly from 
  tg_redis_queue: `from tg_redis_queue.sync_redis_queue import RedisObjectQueue` instead of 
  `from tg_redis_queue import RedisObjectQueue`

## [1.0.0] - 2021-05-28

### Added

- Synchronous redis queue implementation, extracted from existing non-library code
- Tests, and pipeline configuration for running the tests in CI
- Code quality checks and formatters (isort, black, prospector)

[Unreleased]: https://gitlab.com/thorgate-public/tg-redis-queue/-/compare/v1.1.3...master
[1.1.3]: https://gitlab.com/thorgate-public/tg-redis-queue/-/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/thorgate-public/tg-redis-queue/-/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/thorgate-public/tg-redis-queue/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/thorgate-public/tg-redis-queue/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/thorgate-public/tg-redis-queue/-/tags/v1.0.0
